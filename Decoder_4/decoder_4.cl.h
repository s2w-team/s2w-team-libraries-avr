/*    file decoder_4.cl.h
    Author: Sky-WaLkeR @ S2W Team
      Date: modified @ 04.02.2015 22:51 GMT+03
      Repo: S2W Team @ Bitbucket (https://bitbucket.org/Sky-WaLkeR/s2w-libraries-avr)
   License: MIT
        Description:

    Decoder_4 library.

    This simple yet useful library provides class for easy work with decoder-like hardware.
	This version contains library only for 4-channel decoders. As always, you can use separated 
	or non-separated version. When created, all pins are in OUT mode and in LOW state,only if 
	you won't set it manually through constructor parameters

	There are two versions of Decoder_4 class - separated and non-separated.

	Separated version can use pins from any port (created by port.h library).
	Separated template - <PORT_1, PIN_1, PORT_2, PIN_2 ...> and so on up to PIN_4

	Non-separated version can use pins only from one port, but has more simply template.
	Non-separated template - <PORT, PIN_1, PIN_2, PIN_3, PIN_4>

     Methods:
      Decoder_4[_Separated] (uint8_t val=0) where val - initial value of decoder pins
      void set(uint8_t) - set value to decoder. Bit0 == PIN_1, Bit1 == PIN_2 and so on
      void setValue(uint8_t) - just the same
   uint8_t get() - reads state of decoder's pins
   uint8_t getValue() - yeah, the same again

*/

#ifndef _S2W_DECODER4_INCL_
#define _S2W_DECODER4_INCL_

#ifndef _S2W_PORT_INCL_
#error "You should use port.h library, otherwise Decoder_4 won't work"
#endif

template <class PORT_1, uint8_t PIN_1,
		  class PORT_2, uint8_t PIN_2,
		  class PORT_3, uint8_t PIN_3,
		  class PORT_4, uint8_t PIN_4>
class Decoder_4_Separated {
public:
	Decoder_4_Separated(uint8_t init = 0){
		PORT_1::DirSet(1<<PIN_1);
		PORT_1::DirSet(1<<PIN_2);
		PORT_1::DirSet(1<<PIN_3);
		PORT_1::DirSet(1<<PIN_4);

		set(init);
	}

	void set(uint8_t val){
		(val & (1<<0))? PORT_1::Set(1<<PIN_1) : PORT_1::Clear(1<<PIN_1);
		(val & (1<<1))? PORT_2::Set(1<<PIN_2) : PORT_2::Clear(1<<PIN_2);
		(val & (1<<2))? PORT_3::Set(1<<PIN_3) : PORT_3::Clear(1<<PIN_3);
		(val & (1<<3))? PORT_4::Set(1<<PIN_4) : PORT_4::Clear(1<<PIN_4);
	}

	void setValue(uint8_t val) { return set(val); }

	uint8_t get(){
		uint8_t ret=0;
		ret |= (PORT_1::PinRead() & (1<<PIN_1))? 1<<0 : 0;
		ret |= (PORT_2::PinRead() & (1<<PIN_2))? 1<<0 : 0;
		ret |= (PORT_3::PinRead() & (1<<PIN_3))? 1<<0 : 0;
		ret |= (PORT_4::PinRead() & (1<<PIN_4))? 1<<0 : 0;
		return ret;
	}

	uint8_t getValue() { return get(); }
};

template <class PORT, uint8_t PIN_1, uint8_t PIN_2, uint8_t PIN_3, uint8_t PIN_4>
class Decoder_4 : public Decoder_4_Separated <PORT, PIN_1, PORT, PIN_2, PORT, PIN_3, PORT, PIN_4> { };

#endif