# Decoder_4 Library

This simple yet useful library provides class for easy work with decoder-like hardware. This version contains library only for 4-channel decoders. As always, you can use separated or non-separated version. When created, all pins are in OUT mode and in LOW state, only if you won't set it manually through constructor parameters (state, not mode)

# Separated and non-separated

There are two versions of Decoder_4 class - separated and non-separated (Decoder_4_Separated and Decoder_4). Separated version can use pins from any port (created by port.h library). Non-separated version can use pins only from one port, but has more simply template.

## Separated template
    <PORT_1, PIN_1, PORT_2, PIN_2, PORT_3, PIN_3, PORT_4, PIN_4>, where:
     PORT_x - port class created by port.h library
     PIN_x - number of pin in this port

## Non-separated template
    <PORT, PIN_1, PIN_2, PIN_3, PIN_4>

## Decoder_4 methods:

    Decoder_4[_Separated] (uint8_t val=0) where val - initial value of decoder pins

    void set(uint8_t) - set value to decoder. Bit0 == PIN_1, Bit1 == PIN_2 and so on

    void setValue(uint8_t) - just the same

    uint8_t get() - reads state of decoder's pins

    uint8_t getValue() - yeah, the same again

# Example

    #include "port.h"
    #include "decoder_4.cl.h"

    MAKE_PORT(PORTB, DDRB, PINB, Portb);

    int main(){
        Decoder_4 <Portb, 0, 2, 3, 4> decoder(0b1010);
        decoder.set(0b1110);
        while (1) {}
        return 0;
    }

# Dependencies list

port.h S2W library