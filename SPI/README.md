# SPI Library

SPI library comes in two variations - software and hardware.

## Hardware SPI 

Hardware SPI library uses internal SPI module of AVR and supported with almost all ATmega series. Hardware SPI strongly bound to the pins. For example, on ATmega8 hardware SPI is PB2, PB3, PB4 and PB5 pins.

## Software SPI

Software SPI Library don't have any dependencies from internal modules, it's fully software implementation. So, it don't bound to pins, you can choose any. For example, it can be PB0, PB2, PD5 and PD1 for MISO, MOSI, SCK and SS pins, for example. It very useful in large and complex projects or projects with no internal SPI.