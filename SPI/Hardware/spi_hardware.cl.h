/*    file spi_hardware.cl.h
    Author: Sky-WaLkeR @ S2W Team
      Date: modified @ 04.02.2015 21:24 GMT+03
      Repo: S2W Team @ Bitbucket (https://bitbucket.org/Sky-WaLkeR/s2w-libraries-avr)
   License: MIT
        Description:

    SPI Hardware Library.

    This library contains template-based class SPI_Software in namespace SPI, that provides
    easy work with internal SPI module of AVR MCU.

    Detailed description and examples can be found at (url goes here)

     Methods:
      void init(order, sck, edge) - initialization. Calls automatically when object created
      void select() - sets PIN_SS to LOW
      void release() - sets PIN_SS to HIGH
   uint8_t fast_shift(uint8_t) - receives and transmits one byte
      void transmit_sync(uint8_t *dataout, uint8_t len) - transmits data block
      void transfer_sync(uint8_t *out, uint8_t *in, uint8_t len) - receives and transmit data block
*/

#ifndef _S2W_SPI_HARDWARE_INCL_
#define _S2W_SPI_HARDWARE_INCL_

#ifndef _S2W_PORT_INCL_
#error "SPI Hardware Library won't work without 'port' library!"

#include <avr/io.h>
#include "spi_hardware_defs.h"

namespace SPI {
    const uint8_t MSB_FIRST   = 0;
    const uint8_t LSB_FIRST   = 1;
    const uint8_t SCK_IDLE_HI = 1;
    const uint8_t SCK_IDLE_LO = 0;
    const uint8_t EDGE_LEAD   = 0;
    const uint8_t EDGE_TRAIL  = 1;

    template <class PORT, uint8_t PIN_SS>
    class SPI_Hardware {
    public:
        SPI_Hardware(uint8_t order, uint8_t sck, uint8_t edge);
        ~SPI_Hardware() {;}

        void _init(uint8_t order, uint8_t sck, uint8_t edge);

        void select();
        void release();

        uint8_t fast_shift(uint8_t dataout);
        void transmit_sync(uint8_t *dataout, uint8_t len);
        void transfer_sync(uint8_t *dataout, uint8_t * datain, uint8_t len);
    };
}


template <class PORT, uint8_t PIN_SS>
SPI::SPI_Hardware<PORT, PIN_SS>::
SPI_Hardware(uint8_t order = SPI::MSB_FIRST,
             uint8_t sck   = SPI::SCK_IDLE_LO,
             uint8_t edge  = SPI::EDGE_LEAD){
    _init(order, sck, edge);
}

template <class PORT, uint8_t PIN_SS>
void SPI::SPI_Hardware<PORT, PIN_SS>::
select(){
    PORT::Clear(1<<PIN_SS);
}

template <class PORT, uint8_t PIN_SS>
void SPI::SPI_Hardware<PORT, PIN_SS>::
release(){
    PORT::Set(1<<PIN_SS);
}

template <class PORT, uint8_t PIN_SS>
void SPI::SPI_Hardware<PORT, PIN_SS>::
_init(uint8_t order=MSB_FIRST, uint8_t sck=SCK_IDLE_LO, uint8_t edge=EDGE_LEAD){
    SPI_DDR|=1<<SPI_MOSI | 1<<SPI_SCK;
    SPI_DDR&=~(1<<SPI_MISO);
    PORT::DirSet(1<<PIN_SS);
    release();

    SPCR = ((1<<SPE)|            // SPI Enable
            (0<<SPIE)|           // SPI Interrupt Enable
            (order<<DORD)|       // Data Order (0:MSB first / 1:LSB first)
            (1<<MSTR)|           // Master/Slave select
            (0<<SPR1)|(1<<SPR0)| // SPI Clock Rate
            (sck<<CPOL)|         // Clock Polarity (0:SCK low / 1:SCK hi when idle)
            (edge<<CPHA));       // Clock Phase (0:leading / 1:trailing edge sampling)
    SPSR =  (1<<SPI2X);          // Double Clock Rate
}

template <class PORT, uint8_t PIN_SS>
uint8_t SPI::SPI_Hardware<PORT, PIN_SS>::
fast_shift(uint8_t dataout){
    SPDR = dataout;
    while((SPSR & (1<<SPIF))==0);
    return SPDR;
}

template <class PORT, uint8_t PIN_SS>
void SPI::SPI_Hardware<PORT, PIN_SS>::
transmit_sync(uint8_t *dataout, uint8_t len){
    uint8_t i;
    for (i = 0; i < len; i++) {
        SPDR = dataout[i];
    while((SPSR & (1<<SPIF))==0);
    }
}

template <class PORT, uint8_t PIN_SS>
void SPI::SPI_Hardware<PORT, PIN_SS>::
transfer_sync(uint8_t *dataout, uint8_t * datain, uint8_t len){
    uint8_t i;
    for (i = 0; i < len; i++) {
        SPDR = dataout[i];
        while((SPSR & (1<<SPIF))==0);
        datain[i] = SPDR;
    }
}

#endif