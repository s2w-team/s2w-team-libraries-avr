# SPI Hardware Library

This library provides easy work with internal SPI module of AVR MCU.

# Features

*   Auto-init all pins to needed state (HIGH, LOW, IN, OUT)
*   No need to define pins and ports manually. Library knows pins of about 30 AVR MCUs and does it automatically
*   You can set up bit order, idle state of SCK pin and clock phase

# Content

Library wrapped into "SPI" namespace. Library contains one template-based class SPI_Hardware and some of constants for settings.

## SPI_Hardware template

SPI_Hardware uses following template:
    <class PORT, uint8_t PIN_SS>
where:
    PORT - class created by MAKE_PORT from port.h library that points to PORT with SS pin
    PIN_SS - number of SS pin

Example:
    MAKE_PORT(PORTB, DDRB, PINB, Portb);
    SPI_Hardware <Portb, 5> spi;

## SPI_Hardware methods:

    SPI_Hardware(uint8_t order=MSB_FIRST, uint8_t sck=SCK_IDLE_LO, uint8_t edge=EDGE_LEAD)
    Constructor. See "_init" func

    void _init(uint8_t order=MSB_FIRST, uint8_t sck=SCK_IDLE_LO, uint8_t edge=EDGE_LEAD)
     Arguments:
      order - bit order. Can be MSB_FIRST or LSB_FIRST
      sck - idle state of SCK pin. Can be SCK_IDLE_LO or SCK_IDLE_HI
      edge - clock phase. Can be EDGE_TRAIL or EDGE_LEAD
     Description:
      Initializes internal SPI module with given settings and sets pins to needed states. Call automatically from class constructor. Can be used to reinit SPI driver

    void select()
     Description: sets SS pin to LOW (logical 0)

    void release()
     Description: sets SS pin to HIGH (logical 1)

    uint8_t fast_shift(uint8_t out)
     Arguments:
      out - byte of data that will be sent
     Return value: received byte
     Description:
      Writes and reads one byte of data

    void transmit_sync(uint8_t *dataout, uint8_t len)
     Arguments:
      dataout - data that will be sent
      len - amount of data in bytes
     Description:
      Writes array of data

    void transfer_sync(uint8_t *dataout, uint8_t * datain, uint8_t len)
     Arguments:
      dataout - data that will be sent
      datain - received data will be saves here
      len - amount of data in bytes
     Description:
      Writes and reads array of data

# Examples

It's four examples:
*   example1.cpp - simple use as usual class
*   example2.cpp - use as parent for another class
*   example3.cpp - use as parent for another template-based class
*   example4.cpp - use as class inside another template-based class

# Dependencies list

port.h library