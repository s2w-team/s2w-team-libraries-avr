// you should uncomment and set up these lines if your IDE don't do it itself
//#define __AtMega8__
//#define F_CPU 8000000UL

#include <avr/io.h>
#include "port.h" // can be found at https://bitbucket.org/Sky-WaLkeR/s2w-libraries-avr
#include "spi_hardware.cl.h"

MAKE_PORT(PORTB, DDRB, PINB, Portb);
MAKE_PORT(PORTD, DDRD, PIND, Portd);

using namespace SPI;

// notice - when we working with template-based class with SPI as a parent,
// we should use his its methods with this-> prefix
template <class PORT, uint8_t PIN_SS>
class Creature : public SPI_Hardware<PORT, PIN_SS>{
public:
	void init(){
		this->select();
		this->fast_shift(0x31);
		this->release();
	}
};

int main(){
	// create Creature - Hardware SPI, SlaveSelect on PB0
	Creature <Portb, 0> monster1;
	// call method "init"
	monster1.init();

	// do again with another Creature, SlaveSelect on PD5
	Creature <Portd, 5> monster2;
	monster2.init();

	while (1) {}
	return 0;
}