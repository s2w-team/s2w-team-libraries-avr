// you should uncomment and set up these lines if your IDE don't do it itself
//#define __AtMega8__
//#define F_CPU 8000000UL

#include <avr/io.h>
#include "port.h" // can be found at https://bitbucket.org/Sky-WaLkeR/s2w-libraries-avr
#include "spi_hardware.cl.h"

MAKE_PORT(PORTB, DDRB, PINB, Portb);

using namespace SPI;
// create class with SPI_Hardware as a parent with SS=PB0
class Device : public SPI_Hardware<Portb, 0>{
public:
	Device(){
		select();
		fast_shift(0x31); // some "init" procedure
		release();
	}
};

int main(){
	// create Device - Hardware SPI, SlaveSelect on PB0
	Device monster;
	// SPI ready - SPI_Hardware constructor called
	// Device constructor already called too, so our Device is ready to work
	uint8_t bytes[5] = {1,2,3,4,5};
	monster.select();
	monster.transmit_sync(bytes, 5); // send 5 bytes from array
	monster.release();

	while (1) {}
	return 0;
}