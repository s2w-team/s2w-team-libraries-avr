// you should uncomment and set up these lines if your IDE don't do it itself
//#define __AtMega8__
//#define F_CPU 8000000UL

#include <avr/io.h>
#include "port.h" // can be found at https://bitbucket.org/Sky-WaLkeR/s2w-libraries-avr
#include "spi_hardware.cl.h"

MAKE_PORT(PORTB, DDRB, PINB, Portb);
MAKE_PORT(PORTD, DDRD, PIND, Portd);

template <class PORT, uint8_t PIN_SS>
class Creature {
private:
	SPI::SPI_Hardware<PORT, PIN_SS> spi;
public:
	void init(){
		spi.select();
		spi.fast_shift(0x31);
		spi.release();
	}
};

int main(){
	// create Creature - Hardware SPI, SlaveSelect on PB0
	Creature<Portb, 0> monster1;
	monster1.init();

	// another Creature with SlaveSelect on PD5
	Creature<Portd, 5> monster2;
	monster2.init();
	uint8_t arr_in[5], arr_out[5]={9,8,7,6,5};
	monster2.spi.select();
	monster2.spi.transfer_sync(arr_out, arr_in, 5);
	monster2.spi.release();

	while (1) {}
	return 0;
}


