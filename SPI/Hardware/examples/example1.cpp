// you should uncomment and set up these lines if your IDE don't do it itself
//#define __AtMega8__
//#define F_CPU 8000000UL

#include <avr/io.h>
#include "port.h" // can be found at https://bitbucket.org/Sky-WaLkeR/s2w-libraries-avr
#include "spi_hardware.cl.h"

MAKE_PORT(PORTB, DDRB, PINB, Portb, 'B');
MAKE_PORT(PORTD, DDRD, PIND, Portd, 'D');

int main(){
	// create Hardware SPI with SlaveSelect on PB0
	// no need to set up MOSI MOSI SCK defines
	SPI::SPI_Hardware<Portb, 0> driver1;
	// SPI is ready, SS=HIGH
	driver1.select();
	driver1.fast_shift(0x10); // send some byte
	driver1.release();

	// create another driver, SlaveSelect on PD5
	SPI::SPI_Hardware<Portd, 5> driver2;
	driver2.select();
	driver2.fast_shift(0x20);
	driver2.release();
		
	while (1) {}
	return 0;
}
