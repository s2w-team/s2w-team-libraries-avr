// PORT, MOSI, MISO, SCK, SS

// PORTB, 3,4,5,2
//  AVR_ATmega48
//  AVR_ATmega88
//  AVR_ATmega8
//  AVR_ATmega328

// PORTB, 5,6,7,4
//  AVR_ATmega16
//  AVR_ATmega32
//  AVR_ATmega324
//  AVR_ATmega164
//  AVR_ATmega644
//  AVR_ATmega1284
//  AVR_ATmega161
//  AVR_ATmega162
//  AVR_ATmega163

// PORTB, 2,3,1,0
//  AVR_ATmega64
//  AVR_ATmega128
//  AVR_ATmega1280
//  AVR_ATmega640
//  AVR_ATmega1281
//  AVR_ATmega2560
//  AVR_ATmega2561
//  AVR_ATmega645
//  AVR_ATmega325
//  AVR_ATmega103
//  AVR_ATmega3290
//  AVR_ATmega6490
//  AVR_ATmega649
//  AVR_ATmega329

#define SPI_PORT PORTB
#define SPI_DDR  DDRB
#define SPI_PIN  PINB

#if defined (__AVR_ATmega48__) || defined (__AVR_ATmega48A__) || defined (__AVR_ATmega48P__) || defined (__AVR_ATmega88__) || defined (__AVR_ATmega88A__) || \
 	  defined (__AVR_ATmega88P__) || defined (__AVR_ATmega88PA__) || defined (__AVR_ATmega8__) || defined (__AVR_ATmega8U2__) || defined (__AVR_ATmega8HVA__) || \
 	  defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__)
#  define SPI_MOSI 3
#  define SPI_MISO 4
#  define SPI_SCK  5

#elif defined (__AVR_ATmega16__) || defined (__AVR_ATmega16HVA__) || defined (__AVR_ATmega16HVA2__) || defined (__AVR_ATmega16HVB__) || defined (__AVR_ATmega16HVBREVB__) || \
 	  defined (__AVR_ATmega16M1__) || defined (__AVR_ATmega16U2__) || defined (__AVR_ATmega16U4__) || defined (__AVR_ATmega16A__) || defined (__AVR_ATmega32C1__) || \
 	  defined (__AVR_ATmega32M1__) || defined (__AVR_ATmega32U2__) || defined (__AVR_ATmega32U4__) || defined (__AVR_ATmega32U6__) || defined (__AVR_ATmega32__) || \
 	  defined (__AVR_ATmega32HVB__) || defined (__AVR_ATmega32HVBREVB__) || defined (__AVR_ATmega324P__) || defined (__AVR_ATmega324A__) || defined (__AVR_ATmega324PA__) || \
 	  defined (__AVR_ATmega164P__) || defined (__AVR_ATmega164A__) || defined (__AVR_ATmega644__) || defined (__AVR_ATmega644A__) || defined (__AVR_ATmega644P__) || \
 	  defined (__AVR_ATmega644PA__) || defined (__AVR_ATmega1284P__) || defined (__AVR_ATmega161__) || defined (__AVR_ATmega162__) || defined (__AVR_ATmega163__)
#  define SPI_MOSI 5
#  define SPI_MISO 6
#  define SPI_SCK  7

#elif defined (__AVR_ATmega64C1__) || defined (__AVR_ATmega64M1__) || defined (__AVR_ATmega64__) || defined (__AVR_ATmega64HVE__) || defined (__AVR_ATmega128__) || \
 	  defined (__AVR_ATmega128RFA1__) || defined (__AVR_ATmega1280__) || defined (__AVR_ATmega640__) || defined (__AVR_ATmega1281__) || defined (__AVR_ATmega2560__) || \
 	  defined (__AVR_ATmega2561__) || defined (__AVR_ATmega645__) || defined (__AVR_ATmega645A__) || defined (__AVR_ATmega645P__) || defined (__AVR_ATmega325__) || \
 	  defined (__AVR_ATmega325A__) || defined (__AVR_ATmega325P__) || defined (__AVR_ATmega103__) || defined (__AVR_ATmega3290__) || defined (__AVR_ATmega3290A__) || \
 	  defined (__AVR_ATmega3290P__) || defined (__AVR_ATmega6490__) || defined (__AVR_ATmega6490A__) || defined (__AVR_ATmega6490P__) || defined (__AVR_ATmega649__) || \
 	  defined (__AVR_ATmega649A__) || defined (__AVR_ATmega649P__) || defined (__AVR_ATmega329__) || defined (__AVR_ATmega329A__) || defined (__AVR_ATmega329P__) || \
 	  defined (__AVR_ATmega329PA__)
#  define SPI_MOSI 2
#  define SPI_MISO 3
#  define SPI_SCK  1

#else
#  if !defined(SPI_MOSI) || !defined(SPI_MISO) || !defined(SPI_SCK)
#    error "Don't know this MCU, please set defines SPI_MOSI, SPI_MISO and SPI_SCK manually!"
#  endif
#endif