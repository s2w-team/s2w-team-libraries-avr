# S2W Team Libraries for AVR

## S2W Team
S2W Team is a little independent group of programmers and hardware designers. We are part of triangle Labs.

## S2W Libraries

At first S2W Libraries was written only for personal purposes, but now we decided to share it. However, this requires some changes to give it best look and functionality - we need to add comments, fix some bugs and more. It takes some time... 

### Library names

We use postfix ```cl``` for class-based libraries (like ```spi-hardware.cl.h```). Also we have ```opt``` postfix for more lightweight and _opt_imized versions. Some libraries don't have any postfixes (like ```port.h```) because of cow power.

### Content

For now this repository contains the following libraries:

*   __port__ - two classes for easy work with ports and pins. Used in almost all S2W Team Libraries. Idea and 95% of code by Neiver @ easyelectronics.ru
*   __SPI.cl__ - class-based library for SPI protocol
    * Software and hardware implementation
    * Hardware implementation chooses pins for MISO, MOSI and SCK automatically
    * 4 examples for hardware SPI and 4 examples for software SPI included
*   __Decoder_4.cl__ - simple class-based library for 4-pin decoder

These libraries will be added soon:

*   __TWI__ - class-based library for TWI protocol
*   __DS1307__ - work with real-time clock IC DS1307
*   __DS3231__ - work with real-time clock IC DS3231
*   __DHT__ - work with DHT11\DHT22 temperature and humidity sensors
*   __1W__ - driver for 1-Wire protocol
*   __24Cxx__ - work with external EEPROM 21Cxx
*   __NRF24L01__ - driver for NRF24L01(+) 2.4GHz transceiver
*   __RFM42__ - work with RFM42 RF transmitter
*   __HC-05__ - work with HC-05 bluetooth adapter

## Licensing

All these libraries are under MIT license. You can read license terms in LICENSE file. Please, send us an mail if you take this libraries in your projects.

## Contact

If you want to contact with us, you can send a message to Bitbucket user Sky-WaLkeR or email to shadowalker.main@gmail.com 

We will be happy if you help us with some code or entire libraries. We want to create a universal collection for many different things. If you want the same - enjoy us! (Our native language is Russian)

## Contributors

    Sky-WaLkeR
    Walker